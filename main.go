package main

import (
	"flag"
	"log"
	"net/http"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"

	"time"

	"gitlab.com/fawad/dht22_prometheus/sensors"
)

func main() {
	conf := parseConf()
	temperatureGauge := prometheus.NewGauge(prometheus.GaugeOpts{Help: "Current temperature in F", Name: "temperature", Namespace: "climate"})
	humidityGauge := prometheus.NewGauge(prometheus.GaugeOpts{Help: "Current Percent Humidity", Name: "humidity", Namespace: "climate"})

	readingHandler := func(climate sensors.Climate) {
		temperatureF := climate.TemperatureCelcius*9.0/5.0 + 32
		temperatureGauge.Set(float64(temperatureF))
		humidityGauge.Set(float64(climate.HumidityPercentage))
		if conf.debug {
			log.Printf("Temperature = %vF, Humidity = %v%%\n",
				temperatureF, climate.HumidityPercentage)
		}
	}

	s := sensors.Sensor{GpioPort: conf.gpioPort}
	climateChannel := make(chan sensors.Climate)
	go func() {
		for {
			msg := <-climateChannel
			readingHandler(msg)
		}
	}()

	go func() {
		for {
			climate := s.GetClimate()
			climateChannel <- climate
			time.Sleep(time.Duration(conf.pollFrequency) * time.Second)
		}
	}()

	prometheus.MustRegister(temperatureGauge, humidityGauge)
	setupHTTP(conf)
}

func setupHTTP(conf appConf) {
	http.Handle("/metrics", promhttp.Handler())
	log.Println("Metrics available at ", conf.listenAddress)
	log.Fatal(http.ListenAndServe(conf.listenAddress, nil))
}

type appConf struct {
	listenAddress string
	gpioPort      int
	debug         bool
	pollFrequency int
}

func parseConf() appConf {
	addr := flag.String("listen-address", ":8080", "The address to listen on for HTTP requests.")
	port := flag.Int("port", 2, "GPIO Port from which to receive metrics")
	debug := flag.Bool("debug", false, "Enable debugging")
	pollFrequency := flag.Int("poll-frequency", 5, "Frequency in seconds to poll sensor")
	flag.Parse()
	return appConf{listenAddress: *addr, gpioPort: *port, debug: *debug, pollFrequency: *pollFrequency}
}
