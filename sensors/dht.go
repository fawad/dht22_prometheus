// +build linux,arm

package sensors

import (
	"log"

	godht "github.com/d2r2/go-dht"
)

/*
GetClimate retuns a the current climate from the specified port
*/
func (s Sensor) GetClimate() Climate {
	temperature, humidity, _, err :=
		godht.ReadDHTxxWithRetry(godht.DHT22, s.GpioPort, true, 10)
	if err != nil {
		log.Fatal(err)
	}
	return Climate{
		TemperatureCelcius: temperature,
		HumidityPercentage: humidity,
	}
}
