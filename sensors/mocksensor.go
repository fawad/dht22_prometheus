// +build !arm

package sensors

/*
GetClimate retuns a mock climate
*/
func (s Sensor) GetClimate() Climate {
	return Climate{
		TemperatureCelcius: 0.0,
		HumidityPercentage: 0.0,
	}
}
