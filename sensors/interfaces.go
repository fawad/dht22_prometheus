package sensors

type Sensor struct {
	GpioPort int
}

type Climate struct {
	TemperatureCelcius float32
	HumidityPercentage float32
}

type ClimateSource interface {
	GetClimate() Climate
}
